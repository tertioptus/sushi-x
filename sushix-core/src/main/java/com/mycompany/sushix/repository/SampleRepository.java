package com.mycompany.sushix.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mycompany.sushix.model.Sample;

public interface SampleRepository extends MongoRepository<Sample, String> {

}
