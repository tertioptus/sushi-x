package com.mycompany.sushix.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.UrlUtils;
import org.apache.wicket.request.cycle.RequestCycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePage extends WebPage {

    private static final Logger log = LoggerFactory.getLogger(HomePage.class);

    public HomePage() {
        log.debug("Home page!");
        //TODO: Does this always add the ie9.js, even when browser is not IE9?
        add(new WebComponent("ie9js").add(new AttributeModifier("src", new Model<>(UrlUtils.rewriteToContextRelative("js/ie9.js", RequestCycle.get())))));
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        //TODO: Use Wicket 6 resource management to add jquery.
        response.render(JavaScriptHeaderItem.forUrl("js/jquery-1.7.1.min.js"));
        response.render(JavaScriptHeaderItem.forUrl("js/jquery-ui-1.8.17.custom.min.js"));
        response.render(JavaScriptHeaderItem.forUrl("js/plugins.js"));
        response.render(JavaScriptHeaderItem.forUrl("js/scripts.js"));

        response.render(CssHeaderItem.forUrl("css/jquery-ui-1.8.17.custom.css"));
        response.render(CssHeaderItem.forUrl("css/style.css"));
    }
}