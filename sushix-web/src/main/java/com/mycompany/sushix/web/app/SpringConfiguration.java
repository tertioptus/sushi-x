package com.mycompany.sushix.web.app;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyOverrideConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
@ComponentScan(basePackages = "com.mycompany.sushix.web")
public class SpringConfiguration {

    private static final Logger log = LoggerFactory.getLogger(SpringConfiguration.class);

    @Bean
    public static PropertyOverrideConfigurer propertyOvepropertiesrride() {

        final String config = System.getProperty("config");
        if (config == null || config.isEmpty()) {
            log.error("No 'config' system property detected - sushix-web cannot start up.");
            throw new RuntimeException("No 'config' system property detected - sushix-web cannot start up.");
        }

        final File configFile = new File(config, "application.properties");
        if (!configFile.exists() || !configFile.canRead()) {
            log.error("Attempted to load the sushix-web configuration file from {}/application.properties, but either it does not exist or is not readable.", config);
            throw new RuntimeException(String.format("Attempted to load the sushix-web configuration file from %s/application.properties, but either it does not exist or is not readable.", config));
        }

        final PropertyOverrideConfigurer properties = new PropertyOverrideConfigurer();
        properties.setLocation(new FileSystemResource(configFile));
        return properties;
    }
}