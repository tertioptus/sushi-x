package com.mycompany.sushix.web.app;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextCleanupListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class WebAppInitializer implements WebApplicationInitializer {

    private static final Logger log = LoggerFactory.getLogger(WebAppInitializer.class);

    @Override
    public void onStartup(final ServletContext sc) throws ServletException {
        SLF4JBridgeHandler.install();

        log.debug("sushix-web starting up...");

        // Create the 'root' Spring application context
        final AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.register(SpringConfiguration.class);
        ctx.refresh();

        // Register the Spring Context in the ServletContext
        sc.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, ctx);

        // Manages the lifecycle
        sc.addListener(new ContextCleanupListener());

        log.debug("sushix-web initialized.");
    }
}